import Entity from './entity.js';

export default class Meanie extends Entity {

    constructor(position, moves, faces, actor, pattern) {
        super(position, moves, faces, actor);
        this.index = 0;
        this.pattern = pattern;
    }

    update() {
        this.updatePosition();

        this.moves = this.pattern[this.index];
        this.faces = this.moves;
        this.index = (this.index + 1) % this.pattern.length;

        this.updateState();
    }
}
