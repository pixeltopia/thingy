import Entity from './entity.js';
import { up, dn, lf, rt } from './consts.js';

export default class Player extends Entity {

    constructor(position, moves, faces, actor, level) {
        super(position, moves, faces, actor);
        this.level = level;
    }

    update(direction) {
        this.updatePosition();

        let moves = direction;
        let slide = 0;

        const { x, y } = this.position;

        switch (this.level.getValue(x, y)) {
            case this.level.codes.up: slide = up; break;
            case this.level.codes.dn: slide = dn; break;
            case this.level.codes.lf: slide = lf; break;
            case this.level.codes.rt: slide = rt; break;
        }

        if (slide !== 0) {
            moves = slide;
        }

        if (moves !== 0) {
            switch (moves) {
                case up: if (this.level.isSolid(x, y - 1)) moves = 0; break;
                case dn: if (this.level.isSolid(x, y + 1)) moves = 0; break;
                case lf: if (this.level.isSolid(x - 1, y)) moves = 0; break;
                case rt: if (this.level.isSolid(x + 1, y)) moves = 0; break;
            }
        }

        this.moves = moves;

        if (direction !== 0) {
            this.faces = direction;
        }

        if (slide !== 0) {
            this.faces = slide;
        }

        this.updateState(slide);
    }
}
