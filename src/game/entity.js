import {
    up, dn, lf, rt,
    dnIdle,
    dnMove,
    rtIdle,
    rtMove,
    lfIdle,
    lfMove,
    upIdle,
    upMove,
} from './consts.js';

export default class Entity {

    static Point = null;

    constructor(position, moves, faces, actor) {
        this.position = position;
        this.moves = moves;
        this.faces = faces;
        this.actor = actor;
        this.world = Entity.Point.zero;
        this.screen = Entity.Point.zero;
    }

    animate() {
        this.actor.animate();
    }

    updatePosition() {
        switch (this.moves) {
            case up: this.position.y--; break;
            case dn: this.position.y++; break;
            case lf: this.position.x--; break;
            case rt: this.position.x++; break;
        }
    }

    updateState(slide = 0) {
        let state;

        if (this.moves !== 0 && slide === 0) {
            switch (this.moves) {
                case up: state = upMove; break;
                case dn: state = dnMove; break;
                case lf: state = lfMove; break;
                case rt: state = rtMove; break;
            }
        } else {
            switch (this.faces) {
                case up: state = upIdle; break;
                case dn: state = dnIdle; break;
                case lf: state = lfIdle; break;
                case rt: state = rtIdle; break;
            }
        }

        this.actor.setState(state);
    }

    updateWorld(tile, frame) {
        let px = this.position.x * tile;
        let py = this.position.y * tile;

        switch (this.moves) {
            case up: py -= frame; break;
            case dn: py += frame; break;
            case lf: px -= frame; break;
            case rt: px += frame; break;
        }

        this.world.set(px, py);
    }

    updateScreen(viewport) {
        this.screen.set(
            this.world.x - viewport.origin.x,
            this.world.y - viewport.origin.y);
    }

    draw(clipping) {
        if (!clipping.contains(this.screen.x, this.screen.y)) return;
        this.actor.draw(this.screen.x, this.screen.y);
    }
}
