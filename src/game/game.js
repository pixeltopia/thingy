import Entity from './entity.js';
import Player from './player.js';
import Meanie from './meanie.js';
import {
    up, dn, lf, rt,
    dnIdle,
    dnMove,
    rtIdle,
    rtMove,
    lfIdle,
    lfMove,
    upIdle,
    upMove,
} from './consts.js';

export default function (api) {

    const {
        Size,
        Point,
        Rectangle,
        Level,
        Sheet,
        Actor,
        Tiles,
        Font,
    } = api;

    Entity.Point = Point;

    const fps        = 60;
    const delay      = 1;
    const tile       = 16;
    const grid       = new Size(12, 8);
    const screen     = new Rectangle(Point.zero, grid.scaled(tile, tile));
    const increment  = tile / 8;

    const jukebox      = ['tunes/1.txt', 'tunes/2.txt', 'tunes/3.txt', 'tunes/4.txt', 'tunes/5.txt', 'tunes/6.txt'];
    let   jukeboxIndex = 0;

    let frame     = 0;
    let started   = false;
    let ticked    = false;
    let direction = 0;
    let level     = null;
    let sheet     = null;
    let player    = null;
    let meanie    = null;
    let tiles     = null;
    let focused   = null;
    let font      = null;

    let bounds    = null;
    let viewport  = null;
    let clipping  = null;

    function tick() {
        if (!start()) return;

        sample();

        if (api.frame % delay === 0) {
            frame = (frame + increment) % tile;
            if (frame === 0) {
                logic();
                ticked = true;
            }
        }

        if (!ticked) return;

        animate();
        draw();
    }

    function start() {
        if (api.frame === 0) {
            level = new Level('level.txt');
            sheet = new Sheet('sheet.png');
            font = new Font('cushion.fnt', 0x440000ff, 0xffffaaff);
            return false;
        }

        if (!level.ready) return false;
        if (!sheet.ready) return false;
        if (!font.ready) return false;

        if (started) return true;

        started = true;

        const moves = 0;
        const faces = dn;
        const delay = 1000 / fps * 4;

        let sheetTileSize = new Size(16, 16);
        let worldTileSize = new Size(tile, tile);

        let position;

        position = level.players[0];

        player = new Player(
            position, moves, faces,
            new Actor(sheet, sheetTileSize, worldTileSize)
                .setAnimation(dnIdle, delay, [0])
                .setAnimation(dnMove, delay, [1, 2])
                .setAnimation(rtIdle, delay, [3])
                .setAnimation(rtMove, delay, [3, 4])
                .setAnimation(lfIdle, delay, [6])
                .setAnimation(lfMove, delay, [6, 5])
                .setAnimation(upIdle, delay, [7])
                .setAnimation(upMove, delay, [8, 9]),
            level,
        );

        position = level.meanies[0];

        const pattern = [
            dn, dn,
            lf, lf, lf, lf, lf, lf, lf,
            up, up,
            rt, rt, rt, rt, rt, rt, rt,
        ];

        meanie = new Meanie(
            position, moves, faces,
            new Actor(sheet, sheetTileSize, worldTileSize)
                .setAnimation(dnMove, delay, [10, 11])
                .setAnimation(rtMove, delay, [14, 15])
                .setAnimation(lfMove, delay, [16, 17])
                .setAnimation(upMove, delay, [12, 13])
                .setAnimation(dnIdle, delay, [10])
                .setAnimation(rtIdle, delay, [14])
                .setAnimation(lfIdle, delay, [16])
                .setAnimation(upIdle, delay, [12]),
            pattern,
        );

        tiles = new Tiles(sheet, sheetTileSize, level, worldTileSize);

        bounds = new Rectangle(Point.zero, level.bounds.size.scaled(tile, tile));
        viewport = screen.clone;
        clipping = screen.clone;

        clipping.inset(-tile, -tile, 0, 0);

        focused = player;

        return true;
    }

    function sample() {
        direction = 0;
        if (api.input.touch.up && !api.input.touch.dn) direction = up;
        if (api.input.touch.dn && !api.input.touch.up) direction = dn;
        if (api.input.touch.lf && !api.input.touch.rt) direction = lf;
        if (api.input.touch.rt && !api.input.touch.lf) direction = rt;

        if (api.input.press.sp) {
            if (api.music.playing) {
                api.music.stop();
                jukeboxIndex = (jukeboxIndex + 1) % jukebox.length;
            }
            api.music.load(jukebox[jukeboxIndex]);
        }

        if (api.music.ready && !api.music.playing) api.music.play();
    }

    function logic() {
        player.update(direction);
        meanie.update();
    }

    function animate() {
        tiles.animate();
        player.animate();
        meanie.animate();
    }

    function draw() {
        player.updateWorld(tile, frame);
        meanie.updateWorld(tile, frame);

        viewport.setCentre(
            focused.world.x + (tile >> 1),
            focused.world.y + (tile >> 1));
        bounds.limit(viewport);

        player.updateScreen(viewport);
        meanie.updateScreen(viewport);

        tiles.draw(screen, viewport);

        player.draw(clipping);
        meanie.draw(clipping);

        if (api.music.playing) {
            font.draw(`${Font.triangleR} ${jukebox[jukeboxIndex]} ${api.music.bpm} bpm`, 0, 0);
        }
    }

    return { screen, fps, tick };
}
