export const up = 1;
export const dn = 2;
export const lf = 3;
export const rt = 4;

export const upIdle = 1;
export const upMove = 2;
export const dnIdle = 3;
export const dnMove = 4;
export const lfIdle = 5;
export const lfMove = 6;
export const rtIdle = 7;
export const rtMove = 8;
