export default function (aspect, it, inside = document.documentElement) {
    const vw = inside.clientWidth;
    const vh = inside.clientHeight;

    let x = 0;
    let y = 0;
    let w = 0;
    let h = 0;

    if (vw / vh > aspect) {
        h = vh;
        w = vh * aspect;
        x = (vw - w) / 2;
        y = 0;
    } else {
        h = vw / aspect;
        w = vw;
        x = 0;
        y = (vh - h) / 2;
    }

    it.style.width  = `${w}px`;
    it.style.height = `${h}px`;
    it.style.left   = `${x}px`;
    it.style.top    = `${y}px`;
}
