const up = 'ArrowUp';
const dn = 'ArrowDown';
const lf = 'ArrowLeft';
const rt = 'ArrowRight';
const sp = 'Space';

class State {

    constructor() {
        this.clear();
        this.listen('keydown', true);
        this.listen('keyup', false);
    }

    listen(type, value) {
        window.addEventListener(type, (event) => {
            if (event.repeat) return;
            switch (event.code) {
                case up: this.up = value; break;
                case dn: this.dn = value; break;
                case lf: this.lf = value; break;
                case rt: this.rt = value; break;
                case sp: this.sp = value; break;
            }
        });
    }

    clear() {
        this.up = false;
        this.dn = false;
        this.lf = false;
        this.rt = false;
        this.sp = false;
    }
}

export default class Input {

    constructor() {
        this.press = new State();
        this.touch = new State();
    }

    clearPress() {
        this.press.clear();
    }
}
