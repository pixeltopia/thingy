import Animation from './animation.js';
import charCode from './charcode.js';
import {
    Point,
    Rectangle,
    Size,
} from './geometry.js';

export default class Level {

    constructor(url) {
        this.url = url;
        this.ready = false;
        this.error = null;
        this.table = null;
        this.anims = [];
        this.players = [];
        this.meanies = [];
        this.codes = {
            up: charCode('^'),
            dn: charCode('v'),
            lf: charCode('<'),
            rt: charCode('>'),
            player: charCode('P'),
            meanie: charCode('M'),
        };
        fetch(url)
            .then((response) => response.text())
            .then((value) => this.parse(value))
            .catch((error) => this.error = error);
    }

    parse(value) {
        let lines = value.split('\n');

        let flags = new Map();
        let table = new Map();

        while (lines[0].startsWith('T')) {
            const tokens = lines[0].split(' ');
            const [, symbol, flag, ...values] = tokens;
            const code = charCode(symbol);
            const solid = flag === 'solid';
            const delay = values.length > 1 ? Number.parseInt(values.pop()) : 0;
            const frames = values.map((it) => Number.parseInt(it));
            flags.set(code, solid);
            table.set(code, new Animation(delay, frames));
            lines.shift();
        }

        const h = lines.length;
        const w = lines
            .map((it) => it.length)
            .reduce((prev, next) => next > prev ? next : prev);

        this.bounds = new Rectangle(new Point(0, 0), new Size(w, h));
        this.value = new Int16Array(this.bounds.size.area);
        this.solid = new Int16Array(this.bounds.size.area);

        for (let y = 0; y < h; y++) {
            const line = lines[y];
            for (let x = 0; x < w; x++) {
                if (x === line.length) break;
                const code = charCode(line, x);
                if (!flags.has(code) && !table.has(code)) continue;
                this.setValue(x, y, code);
                this.setSolid(x, y, flags.get(code));
                if (code === this.codes.player) this.players.push(new Point(x, y));
                if (code === this.codes.meanie) this.meanies.push(new Point(x, y));
            }
        }

        this.anims = [...table.values()];
        this.table = table;
        this.ready = true;
    }

    animate(step) {
        for (let index = 0; index < this.anims.length; index++) {
            this.anims[index].animate(step);
        }
    }

    isValid(x, y) {
        return this.bounds.contains(x, y);
    }

    setValue(x, y, value) {
        this.value[this.index(x, y)] = value;
    }

    getValue(x, y) {
        return this.value[this.index(x, y)];
    }

    getSheetIndex(x, y) {
        let value = this.getValue(x, y);

        if (this.table.has(value)) {
            value = this.table.get(value).frame;
        }

        return value;
    }

    setSolid(x, y, solid) {
        this.solid[this.index(x, y)] = solid ? 1 : 0;
    }

    isSolid(x, y) {
        return this.solid[this.index(x, y)] === 1;
    }

    index(x, y) {
        return y * this.bounds.w + x;
    }
}
