export default class Animation {

    constructor(delay, frames) {
        this.delay = delay;
        this.frames = frames;
        this.reset();
    }

    get frame() {
        return this.frames[this.index];
    }

    reset() {
        this.index = 0;
        this.cycle = 0;
        this.delta = 0;
    }

    animate(step) {
        this.delta += step;
        if (this.delta < this.delay) return;
        this.delta = 0;
        this.index++;
        if (this.index < this.frames.length) return;
        this.index = 0;
        this.cycle++;
    }
}
