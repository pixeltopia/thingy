export default class Message {

    constructor(sheet, sheetTileSize, font, tileIndexes) {
        this.sheet = sheet;
        this.sheetTileSize = sheetTileSize;
        this.font = font;
        this.tileIndexes = tileIndexes;
    }

    draw(text) {
        const {t, b, l, r, tl, tr, bl, br, middle, shadow} = this.tileIndexes;
    }
}
