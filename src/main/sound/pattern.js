import Track from './track.js';

export default class Pattern {

    constructor(name) {
        this.name = name;
        this.tracks = [
            new Track(),
            new Track(),
            new Track(),
            new Track(),
        ];
    }

    get length() {
        return this.tracks[0].length;
    }

    add(track, note, octave, effect, arpeggio) {
        this.tracks[track].add(note, octave, effect, arpeggio);
    }
}
