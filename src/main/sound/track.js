export default class Track {

    constructor(wave = 'square') {
        this.wave      = wave;
        this.notes     = [];
        this.octaves   = [];
        this.effects   = [];
        this.arpeggios = [];
    }

    get length() {
        return this.notes.length;
    }

    add(note, octave, effect, arpeggio) {
        this.notes.push(note);
        this.octaves.push(octave);
        this.effects.push(effect);
        this.arpeggios.push(arpeggio);
    }
}
