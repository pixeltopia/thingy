export default class Arpeggio {

    constructor(name, speed, values) {
        this.name = name;
        this.speed = speed;
        this.values = values;
    }

    get seconds() {
        return this.speed / 1000.0;
    }

    value(index) {
        return this.values[index % this.values.length];
    }
}
