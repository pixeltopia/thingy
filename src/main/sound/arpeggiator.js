import frequency from './frequency.js';

export default class Arpeggiator {

    constructor(src) {
        this.src = src;
        this.reset();
    }

    get arpeggiating() {
        return this.nextTime !== null;
    }

    reset() {
        this.note     = null;
        this.octave   = null;
        this.arpeggio = null;
        this.nextTime = null;
        this.index    = 0;
    }

    updateNextTime(time) {
        this.nextTime = time + this.arpeggio.seconds;
    }

    updateFrequency() {
        this.src.frequency.value = frequency(
            this.note,
            this.octave,
            this.arpeggio.value(this.index));
    }

    play(time, note, octave, arpeggio) {
        this.note = note;
        this.octave = octave;
        this.arpeggio = arpeggio;
        this.index = 0;
        this.updateNextTime(time);
        this.updateFrequency();
    }

    tick(time) {
        if (!this.arpeggiating || time < this.nextTime) return;
        this.index++;
        this.updateNextTime(time);
        this.updateFrequency();
    }
}
