export default class Song {

    constructor() {
        this.waves     = [];
        this.bpm       = 0;
        this.step      = 0;
        this.sequence  = [];
        this.patterns  = {};
        this.arpeggios = {};
    }

    setBpm(newValue) {
        this.bpm  = newValue;
        this.step = 60 / newValue;
    }

    addPattern(pattern) {
        this.patterns[pattern.name] = pattern;
    }

    addArpeggio(arpeggio) {
        this.arpeggios[arpeggio.name] = arpeggio;
    }
}
