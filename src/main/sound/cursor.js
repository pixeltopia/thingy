class Index {

    constructor() {
        this.rewind();
    }

    rewind() {
        this.pattern = 0;
        this.row = 0;
    }

    step(patternLength, sequenceLength) {
        this.row++;
        if (this.row === patternLength) {
            this.row = 0;
            this.pattern++;
        }
        if (this.pattern === sequenceLength) {
            this.pattern = 0;
        }
    }
}

class Head {

    constructor() {
        this.reset();
    }

    reset() {
        this.note     = null;
        this.octave   = null;
        this.effect   = null;
        this.arpeggio = null;
    }

    read(track, row) {
        this.note     = track.notes[row];
        this.octave   = track.octaves[row];
        this.effect   = track.effects[row];
        this.arpeggio = track.arpeggios[row];
    }
}

export default class Cursor {

    constructor() {
        this.index = new Index();
        this.heads = [
            new Head(),
            new Head(),
            new Head(),
            new Head(),
        ];
    }

    rewind() {
        this.index.rewind();
        this.heads.forEach((it) => it.reset());
    }

    read({ patterns, sequence }) {
        const { tracks, length } = patterns[sequence[this.index.pattern]];
        for (let index = 0; index < tracks.length; index++) {
            this.heads[index].read(tracks[index], this.index.row);
        }
        this.index.step(length, sequence.length);
    }
}
