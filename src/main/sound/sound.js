import Noise      from './noise.js';
import Voice      from './voice.js';
import { Volume } from './volume.js';

export default class Sound {

    constructor() {
        this.ctx        = new AudioContext();
        this.scratch    = this.ctx.createBuffer(1, 1, 22050);
        this.startTime  = -1;
        this.unlocker   = this.unlock.bind(this);
        this.master     = this.ctx.createGain();
        this.volume     = new Volume(this.master, 1, false);

        this.master.connect(this.ctx.destination);

        if (this.ctx.state === 'suspended') {
            document.addEventListener('keydown', this.unlocker, true);
        } else {
            this.startTime = this.ctx.currentTime;
        }

        this.voices = [
            new Noise(this.ctx, this.master, this.scratch),
            new Voice(this.ctx, this.master, 'triangle'),
            new Voice(this.ctx, this.master, 'triangle'),
            new Voice(this.ctx, this.master, 'triangle'),
        ];
    }

    unlock() {
        const src = this.ctx.createBufferSource();
        src.buffer = this.scratch;
        src.connect(this.ctx.destination);
        src.onended = () => {
            this.startTime = this.ctx.currentTime;
            src.disconnect(0);
            document.removeEventListener('keydown', this.unlocker, true);
        };
        src.start(0);
        if (this.ctx.state === 'suspended') this.ctx.resume();
    }

    get time() {
        return this.startTime === -1 ? 0 : this.ctx.currentTime - this.startTime;
    }

    get arpeggiating() {
        for (let index = 0; index < this.voices.length; index++) {
            if (this.voices[index].arpeggiating) return true;
        }
        return false;
    }

    tick() {
        if (this.startTime === -1) return;
        for (let index = 0; index < this.voices.length; index++) {
            this.voices[index].tick(this.time);
        }
    }

    setVoices(...waves) {
        this.voices.forEach((voice) => voice.teardown());
        this.voices = waves.map((wave) => {
            return wave === 'noise'
                ? new Noise(this.ctx, this.master, this.scratch)
                : new Voice(this.ctx, this.master, wave);
        });
    }

    play(channel, note, octave, length, vibrato, arpeggio, gain = 1) {
        this.voices[channel].play(this.time, note, octave, length, vibrato, arpeggio, gain);
    }
}
