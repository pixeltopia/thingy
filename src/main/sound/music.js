import Pattern  from './pattern.js';
import Song     from './song.js';
import Cursor   from './cursor.js';
import Arpeggio from './arpeggio.js';

export default class Music {

    constructor(sound) {
        this.sound  = sound;
        this.ready  = false;
        this.error  = null;
        this.song   = null;
        this.last   = null;
        this.cursor = new Cursor();
    }

    get playing() {
        return this.last !== null;
    }

    get bpm() {
        return this.song.bpm;
    }

    get quarterBeats() {
        return this.song.step / 4;
    }

    load(url) {
        this.ready = false;
        this.error = null;
        this.song  = null;
        fetch(url)
            .then((response) => response.text())
            .then((value) => this.parse(value))
            .catch((error) => this.error = error);
    }

    parse(value) {
        let lines   = value.split('\n');
        let song    = new Song();
        let pattern = null;

        function parseNote(track, note, effect, arpeggio) {
            if (note === '---') {
                pattern.add(track, null, null, null, null);
                return;
            }
            let octave = Number.parseInt(note.charAt(note.length - 1));
            if (Number.isNaN(octave)) octave = null;
            note = note.slice(0, 2);
            if (note.endsWith('-')) note = note.charAt(0);
            if (effect === '-') effect = null;
            if (arpeggio === '--') arpeggio = null;
            pattern.add(track, note, octave, effect, arpeggio);
        }

        for (let index = 0; index < lines.length; index++) {
            const line = lines[index];

            if (line.startsWith('#') || line.length === 0) continue;

            if (line.startsWith('WAV')) {
                const [, ...waves] = line.split(' ');
                song.waves.push(...waves);
                continue;
            }

            if (line.startsWith('BPM')) {
                const [, bpm] = line.split(' ');
                song.setBpm(Number.parseInt(bpm));
                continue;
            }

            if (line.startsWith('SEQ')) {
                const [, ...sequence] = line.split(' ');
                song.sequence.push(...sequence);
                continue;
            }

            if (line.startsWith('PAT')) {
                if (pattern) song.addPattern(pattern);
                const [, name] = line.split(' ');
                pattern = new Pattern(name);
                continue;
            }

            if (line.startsWith('ARP')) {
                const [, name, speed, pattern] = line.split(' ');
                const values = pattern
                    .split('')
                    .map((it) => Number.parseInt(it));
                song.addArpeggio(new Arpeggio(name, Number.parseInt(speed), values));
                continue;
            }

            if (!pattern) continue;

            const [
                note0, effect0, arpeggio0,
                note1, effect1, arpeggio1,
                note2, effect2, arpeggio2,
                note3, effect3, arpeggio3,
            ] = line.split(' ');

            parseNote(0, note0, effect0, arpeggio0);
            parseNote(1, note1, effect1, arpeggio1);
            parseNote(2, note2, effect2, arpeggio2);
            parseNote(3, note3, effect3, arpeggio3);
        }

        if (pattern) song.addPattern(pattern);

        this.sound.setVoices(...song.waves);
        this.rewind();

        this.song   = song;
        this.ready  = true;
    }

    play() {
        this.last = this.sound.time;
    }

    stop() {
        this.last = null;
    }

    rewind() {
        this.cursor.rewind();
    }

    tick() {
        if (this.last === null) return;

        const now = this.sound.time;

        if (now - this.last < this.quarterBeats) return;

        this.last = now;

        this.cursor.read(this.song);

        for (let index = 0; index < this.cursor.heads.length; index++) {
            const { note, octave, effect, arpeggio } = this.cursor.heads[index];
            if (note === null) continue;
            this.sound.play(
                index,
                note,
                octave,
                this.quarterBeats,
                effect === 'V',
                this.song.arpeggios[arpeggio]);
        }
    }
}
