export class Volume {

    constructor(node, value, muted) {
        this.currentValue = 0;
        this.currentMuted = false;
        this.node = node;
        this.value = value;
        this.muted = muted;
    }

    get value() {
        return this.currentValue;
    }

    set value(newValue) {
        this.currentValue = newValue;
        this.update();
    }

    get muted() {
        return this.currentMuted;
    }

    set muted(newMuted) {
        this.currentMuted = newMuted;
        this.update();
    }

    update() {
        if (this.currentMuted) {
            this.node.gain.value = 0;
        } else {
            this.node.gain.value = this.currentValue;
        }
    }
}
