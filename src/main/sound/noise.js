export default class Noise {

    constructor(ctx, dst, scratch) {
        this.ctx = ctx;
        this.src = ctx.createBufferSource();

        const size = Math.floor(ctx.sampleRate * 0.5);
        const buff = ctx.createBuffer(1, size, ctx.sampleRate);
        const data = buff.getChannelData(0);

        for (let index = 0; index < size; index++) {
            data[index] = Math.random() * 2 - 1;
        }

        this.src.buffer = buff;
        this.src.loop = true;
        this.src.start();

        this.amp = ctx.createGain();
        this.amp.gain.value = 0;

        this.src.connect(this.amp);
        this.amp.connect(dst);

        this.scratch = scratch;

        this.stopsAtTime = -1;
    }

    teardown() {
        this.src.stop();
        this.amp.disconnect();
        this.src.disconnect();
    }

    get arpeggiating() {
        return false;
    }

    play(time, note, octave, length, vibrato, gain = 1) {
        this.amp.gain.value = gain;
        this.stopsAtTime = time + length;
    }

    mute() {
        this.amp.gain.value = 0;
    }

    tick(time) {
        if (this.stopsAtTime === -1 || time < this.stopsAtTime) return;
        this.mute();
        this.stopsAtTime = -1;
    }
}
