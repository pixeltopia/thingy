import frequency   from './frequency.js';
import Vibrato     from './vibrato.js';
import Arpeggiator from './arpeggiator.js';

export default class Voice {

    constructor(ctx, dst, wave = 'square') {
        this.ctx = ctx;
        this.src = ctx.createOscillator();
        this.src.type = wave;
        this.src.frequency.value = 0;
        this.src.start(0);

        this.amp = ctx.createGain();
        this.amp.gain.value = 0;

        this.src.connect(this.amp);
        this.amp.connect(dst);

        this.stopsAtTime = -1;

        this.vib = new Vibrato(ctx);
        this.vib.attach(this);

        this.arpeggiator = new Arpeggiator(this.src);
    }

    teardown() {
        this.vib.teardown();
        this.src.stop();
        this.amp.disconnect();
        this.src.disconnect();
    }

    get arpeggiating() {
        return this.arpeggiator.arpeggiating;
    }

    play(time, note, octave, length, vibrato, arpeggio, gain = 1) {
        this.amp.gain.value = gain;
        this.stopsAtTime = time + length;
        if (vibrato) {
            this.vib.on();
        } else {
            this.vib.off();
        }
        if (arpeggio) {
            this.arpeggiator.play(time, note, octave, arpeggio);
        } else {
            this.src.frequency.value = frequency(note, octave);
        }
    }

    mute() {
        this.amp.gain.value = 0;
    }

    tick(time) {
        this.arpeggiator.tick(time);
        if (this.stopsAtTime === -1 || time < this.stopsAtTime) return;
        this.vib.off();
        this.mute();
        this.arpeggiator.reset();
        this.stopsAtTime = -1;
    }
}
