export default class Vibrato {
    constructor(ctx, level = 10) {
        this.ctx = ctx;
        this.src = ctx.createOscillator();
        this.amp = ctx.createGain();

        this.src.frequency.value = level;
        this.src.connect(this.amp);
        this.src.start(0);
    }

    teardown() {
        this.src.stop();
        this.amp.disconnect();
        this.src.disconnect();
    }

    attach(voice) {
        this.amp.connect(voice.src.detune);
    }

    on() {
        this.amp.gain.value = 50;
    }

    off() {
        this.amp.gain.value = 0;
    }
}
