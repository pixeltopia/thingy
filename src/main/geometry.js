export class Point {

    static get zero() {
        return new Point(0, 0);
    }

    constructor(x, y) {
        this.set(x, y);
    }

    get clone() {
        return new Point(this.x, this.y);
    }

    set(x, y) {
        this.x = x;
        this.y = y;
    }

    offset(dx, dy) {
        this.x += dx;
        this.y += dy;
    }
}

export class Rectangle {

    constructor(origin, size) {
        this.origin = origin;
        this.extent = Point.zero;
        this.w = size.w;
        this.h = size.h;
    }

    get clone() {
        return new Rectangle(this.origin.clone, this.size);
    }

    get w() {
        return this.extent.x - this.origin.x + 1;
    }

    get h() {
        return this.extent.y - this.origin.y + 1;
    }

    set w(value) {
        this.extent.x = this.origin.x + value - 1;
    }

    set h(value) {
        this.extent.y = this.origin.y + value - 1;
    }

    get size() {
        return new Size(this.w, this.h);
    }

    offset(dx, dy) {
        this.origin.offset(dx, dy);
        this.extent.offset(dx, dy);
    }

    inset(t, l, b, r) {
        this.origin.y += t;
        this.origin.x += l;
        this.extent.y -= b;
        this.extent.x -= r;
    }

    setCentre(x, y) {
        const hw = this.w >> 1;
        const hh = this.h >> 1;
        const dx = this.w & 1 === 1 ? 0 : 1;
        const dy = this.h & 1 === 1 ? 0 : 1;
        this.origin.x = x - hw;
        this.origin.y = y - hh;
        this.extent.x = x + hw - dx;
        this.extent.y = y + hh - dy;
    }

    limit(other) {
        let d;

        d = other.origin.x - this.origin.x;
        if (d < 0) other.offset(-d, 0);

        d = other.extent.x - this.extent.x;
        if (d > 0) other.offset(-d, 0);

        d = other.origin.y - this.origin.y;
        if (d < 0) other.offset(0, -d);

        d = other.extent.y - this.extent.y;
        if (d > 0) other.offset(0, -d);
    }

    contains(x, y) {
        return this.origin.x <= x && x <= this.extent.x &&
               this.origin.y <= y && y <= this.extent.y;
    }
}

export class Size {

    constructor(w, h) {
        this.w = w;
        this.h = h;
    }

    get area() {
        return this.w * this.h;
    }

    scaled(sx, sy) {
        return new Size(this.w * sx, this.h * sy);
    }
}
