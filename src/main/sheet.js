export default class Sheet {

    static context = null;

    constructor(url) {
        this.image = new Image();
        this.ready = false;
        this.error = null;

        this.image.onload = () => this.ready = true;
        this.image.onerror = (error) => this.error = error;

        this.image.src = url;
    }

    draw(tileSize, x, y, w, h, index) {
        const tilesAcross = Math.floor(this.image.width / tileSize.w);
        Sheet.context.drawImage(
            this.image,
            Math.floor(index % tilesAcross) * tileSize.w,
            Math.floor(index / tilesAcross) * tileSize.h,
            tileSize.w,
            tileSize.h,
            x, y, w, h);
    }
}
