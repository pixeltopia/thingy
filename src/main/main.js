import Input from './input.js';
import Sound from './sound/sound.js';
import Music from './sound/music.js';
import Level from './level.js';
import Sheet from './sheet.js';
import Tiles from './tiles.js';
import Actor from './actor.js';
import Font from './font.js';
import makeContext from './context.js';
import {
    Point,
    Rectangle,
    Size,
} from './geometry.js';

export default function (game) {
    window.addEventListener('load', () => {
        const input = new Input();
        const sound = new Sound();
        const music = new Music(sound);

        const api = {
            Size,
            Point,
            Rectangle,
            Level,
            Sheet,
            Tiles,
            Actor,
            Font,
            input,
            sound,
            music,
            frame: 0,
        };

        const { screen, fps, tick } = game(api);

        const step = 1000 / fps;

        const context = makeContext(screen.size);

        Sheet.context = context;
        Font.context  = context;
        Tiles.step    = step;
        Actor.step    = step;

        let last = null;
        let accumulated = 0;
        let delta = 0;

        let handle = null;

        function loop(now) {
            last = last || now;
            delta = now - last;
            last = now;
            accumulated += delta;
            while (accumulated >= step) {
                tick();
                api.sound.tick();
                api.music.tick();
                api.input.clearPress();
                api.frame += 1;
                accumulated -= step;
            }
            if (!document.hidden) {
                handle = window.requestAnimationFrame(loop);
            }
        }

        function check() {
            api.sound.volume.muted = document.hidden;
            if (document.hidden) {
                last = null;
                if (handle !== null) {
                    window.cancelAnimationFrame(handle);
                    handle = null;
                }
            } else {
                handle = window.requestAnimationFrame(loop);
            }
        }

        document.addEventListener('visibilitychange', check);
        check();
    });
};
