import charCode from './charcode.js';

export default class Font {

    static heart        = '\u2665';
    static ace          = '\u2660';
    static club         = '\u2663';
    static diamond      = '\u2666';
    static bullet       = '\u2022';
    static escape       = '\u241b';
    static upArrow      = '\u2b06';
    static dnArrow      = '\u2b07';
    static lfArrow      = '\u2b05';
    static rtArrow      = '\u27a1';
    static ulArrow      = '\u2196';
    static diagonalTRBL = '\u2571';
    static diagonalTLBR = '\u2572';
    static triangleTRBL = '\u25e2';
    static triangleTLBR = '\u25e3';
    static triangleL    = '\u25c0';
    static triangleR    = '\u25b6';
    static boxTCRB      = '\u2523';
    static boxTLC       = '\u251b';
    static boxTLCB      = '\u252b';
    static boxLCB       = '\u2513';
    static boxCRB       = '\u250f';
    static boxLCR       = '\u2501';
    static boxTLCRB     = '\u254b';
    static boxLCRB      = '\u2533';
    static boxTLCR      = '\u253b';
    static boxTCR       = '\u2517';
    static boxTCB       = '\u2503';
    static blockBR      = '\u2597';
    static blockTR      = '\u259d';
    static blockTL      = '\u2598';
    static blockBL      = '\u2596';
    static blockBLBR    = '\u2584';
    static blockTLBL    = '\u258c';
    static edgeT        = '\u2594';
    static edgeL        = '\u258f';
    static edgeR        = '\u2595';
    static edgeB        = '\u2581';

    static context = null;

    constructor(name, back, fore) {
        this.name = name;
        this.ready = false;
        fetch('charmap.txt')
            .then((response) => response.text())
            .then((charmap) => {
                fetch(name)
                    .then((response) => response.arrayBuffer())
                    .then((buffer) => new Int8Array(buffer))
                    .then((bytes) => this.setup(charmap, bytes, back, fore));
            });
    }

    setup(charmap, bytes, back, fore) {
        this.setupCharmap(charmap);
        this.back = this.makeGraphic(bytes, back);
        this.fore = this.makeGraphic(bytes, fore);
        this.ready = true;
    }

    setupCharmap(charmap) {
        const map = new Map();

        const invalid = charCode('~');

        for (let index = 0; index < charmap.length; index++) {
            const char = charCode(charmap, index);
            if (char === invalid) continue;
            map.set(char, index << 3);
        }

        this.charmap = map;
    }

    makeGraphic(bytes, colour) {
        const chars = bytes.length >> 3;
        let   scan  = chars << 5;
        const rgba  = new Uint8ClampedArray(scan << 3);

        const r = (colour >> 24) & 0xff;
        const g = (colour >> 16) & 0xff;
        const b = (colour >>  8) & 0xff;
        const a = (colour >>  0) & 0xff;

        scan -= 32;

        for (let char = 0; char < chars; char++) {
            let src = char << 3;
            let dst = char << 5;

            for (let row = 0; row < 8; row++) {
                for (let col = 0; col < 8; col++) {
                    if (((bytes[src] << col) & 0b10000000) !== 0) {
                        rgba[dst++] = r;
                        rgba[dst++] = g;
                        rgba[dst++] = b;
                        rgba[dst++] = a;
                    } else {
                        dst += 4;
                    }
                }
                dst += scan;
                src++;
            }
        }

        const w = chars << 3;
        const h = 8;

        const canvas  = document.createElement('canvas');
        const context = canvas.getContext('2d');
        canvas.width  = w;
        canvas.height = h;

        context.putImageData(new ImageData(rgba, w, h), 0, 0);

        return canvas;
    }

    draw(text, x, y, w = 8, h = 8) {
        let char, sx, sy = 0;
        for (let index = 0; index < text.length; index++, x += w) {
            char = charCode(text, index);
            if (!this.charmap.has(char)) continue;
            sx = this.charmap.get(char);
            Font.context.drawImage(this.back, sx, sy, 8, 8, x + 1, y + 1, w, h);
            Font.context.drawImage(this.fore, sx, sy, 8, 8, x, y, w, h);
        }
    }
}
