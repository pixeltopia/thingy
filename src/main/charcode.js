export default function (string, index = 0) {
    return string.charCodeAt(index);
}
