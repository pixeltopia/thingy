export default class Tiles {

    static step = 0;

    constructor(sheet, sheetTileSize, level, size) {
        this.sheet = sheet;
        this.sheetTileSize = sheetTileSize;
        this.level = level;
        this.size = size;
    }

    animate(step = Tiles.step) {
        this.level.animate(step);
    }

    draw(display, viewport) {
        const x = display.origin.x;
        const y = display.origin.y;
        const w = display.w;
        const h = display.h;

        const sx = viewport.origin.x;
        const sy = viewport.origin.y;

        const tileW = this.size.w;
        const tileH = this.size.h;
        const offsetX = sx % tileW;
        const offsetY = sy % tileH;
        const originX = Math.floor(sx / tileW);
        const originY = Math.floor(sy / tileW);
        const cols = Math.floor(w / tileW) + (offsetX === 0 ? 0 : 1);
        const rows = Math.floor(h / tileH) + (offsetY === 0 ? 0 : 1);

        let row, col, tx, ty, px, py = y - offsetY;

        for (row = 0; row < rows; row++, py += tileH) {
            px = x - offsetX;
            for (col = 0; col < cols; col++, px += tileW) {
                tx = originX + col;
                ty = originY + row;
                if (!this.level.isValid(tx, ty)) continue;
                this.sheet.draw(
                    this.sheetTileSize,
                    px,
                    py,
                    tileW,
                    tileH,
                    this.level.getSheetIndex(tx, ty));
            }
        }
    }
}
