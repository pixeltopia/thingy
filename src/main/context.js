import fit from './fit.js';

export default function ({ w, h }) {
    const canvas  = document.createElement('canvas');
    const context = canvas.getContext('2d');

    canvas.width  = w;
    canvas.height = h;

    context.setTransform(1, 0, 0, 1, 0, 0);
    context.imageSmoothingEnabled = false;

    document.body.appendChild(canvas);

    canvas.style.position = 'relative';
    canvas.style.imageRendering = 'pixelated';

    const aspect = w / h;
    window.addEventListener('resize', () => fit(aspect, canvas));
    fit(aspect, canvas);

    return context;
}
