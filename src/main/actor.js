import Animation from './animation.js';

export default class Actor {

    static step = 0;

    constructor(sheet, sheetTileSize, size) {
        this.sheet = sheet;
        this.sheetTileSize = sheetTileSize;
        this.size = size;
        this.anims = {};
        this.state = 0;
        this.index = 0;
        this.cycle = 0;
        this.delta = 0;
    }

    get anim() {
        return this.anims[this.state];
    }

    setAnimation(state, delay, frames) {
        this.anims[state] = new Animation(delay, frames);
        return this;
    }

    setState(state, force = false) {
        if (this.state === state && !force) return;
        this.state = state;
        this.anim.reset();
        return this;
    }

    animate(step = Actor.step) {
        this.anim.animate(step);
        return this;
    }

    draw(x, y) {
        this.sheet.draw(
            this.sheetTileSize,
            x, y,
            this.size.w,
            this.size.h,
            this.anim.frame);
        return this;
    }
}
