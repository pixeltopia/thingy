# Thingy

This is a top-down two dimensional game. It's written in Javascript for Canvas. To try and ensure this game can be played in the future it's been written with a minimalist approach with the following things in mind:

- plain old JavaScript
- no dependencies or build process
- standard browser apis

To aid with longevity and ease of porting to other languages (such as Java, Kotlin, Swift, C++), the code has been written with the following things in mind:

- logic is insulated from browser apis
- code is written in a portable way, classes, functions, arrays

## Fonts

This code uses a fixed-width bitmap font drawing routine that reads 8-bit Atari fonts ordered in [ATASCII](https://en.wikipedia.org/wiki/ATASCII) order. I've used some [ZX Origins fonts made by DamienG](https://damieng.com/typography/zx-origins/). There is a branch of this project that loads and draws web fonts, but characters were anti-aliased and looked blurred at low resolution.

Some of the special ATASCII symbols have been mapped to their equivalent unicode values and there are static properties that can be used to draw these characters as strings.

## Drawing with fonts

The ATASCII character set has [Box Drawing Characters](https://en.wikipedia.org/wiki/Box-drawing_character) so these have been mapped to unicode characters, and static properties have been defined for these characters.

### Boxes

Box drawing characters have static properties with a naming convention based on grids, T = top, L = left, C = centre, R = right, B = bottom where the letters are written in row then column order.

    +---+---+---+ +---+---+---+
    |   | T |   | |   | # |   |
    +---+---+---+ +---+---+---+
    | L | C | R | | # | # |   | = boxTLCB
    +---+---+---+ +---+---+---+
    |   | B |   | |   | # |   |
    +---+---+---+ +---+---+---+

### Blocks

Block characters also have a grid naming convention, TL = top-left, TR = top-right, BL = bottom-left, BR = bottom-right

    +----+----+ +---+---+
    | TL | TR | | # |   |
    +----+----+ +---+---+ = blockTLBL
    | BL | BR | | # |   |
    +----+----+ +---+---+

Note: The ATASCII character set doesn't have characters for `TLTR` or `TRBR` because the Atari computers could invert character colours as a space-saving technique (white becomes black and vice versa) which would give an effect of flipping the blocks vertically and horizontally. The font routines currently don't support this colour effect, so you've only got `TLBL` and `BLBR` to play with.

### Quarter blocks

Quarter blocks characters have a naming convention based on edges, T, L, R, B. These characters are a quarter of a character.

    +- T -+ +-----+ = edgeT
    |     |
    L     R
    |     |
    +- B -+

Unicode doesn't have characters for upper and right quarter blocks, so quarter blocks have been mapped to unicode eighth blocks, which do have upper and right blocks.

## Running

Double click the index.html file or you can start a local web server. If you've installed ruby you can start a server with:

    ruby -run -e httpd . -p 1234
